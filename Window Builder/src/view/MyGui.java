package view;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.JTextPane;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import javax.swing.JPanel;
import java.awt.FlowLayout;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.DropMode;

public class MyGui {

	private JFrame frame;
	private JTextField txtTextEinfgen;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {

			public void run() {
				try {
					MyGui window = new MyGui();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

/** 

 * Create the application. 

 */ 

public MyGui() { 
initialize(); 
}
	/**
	 * 
	 * Initialize the contents of the frame.
	 * 
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(200, 200, 400, 800);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		JButton btnRot = new JButton("Rot");
		btnRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel.setBackground(Color.RED);
			}
		});

		btnRot.setBounds(22, 178, 93, 25);
		panel.add(btnRot);
		JButton btnGrn = new JButton("Gr\u00FCn");
		btnGrn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel.setBackground(Color.GREEN);
			}
		});
		btnGrn.setBounds(127, 178, 115, 25);
		panel.add(btnGrn);
		JButton btnBlau = new JButton("Blau");
		btnBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel.setBackground(Color.BLUE);
			}
		});
		btnBlau.setBounds(254, 178, 111, 25);
		panel.add(btnBlau);
		JButton btnGelb = new JButton("Gelb");
		btnGelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel.setBackground(Color.YELLOW);
			}
		});
		btnGelb.setBounds(22, 227, 93, 25);
		panel.add(btnGelb);
		JButton btnStandardfarbe = new JButton("Standardfarbe");
		btnStandardfarbe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel.setBackground(null);
			}
		});
		btnStandardfarbe.setBounds(127, 227, 115, 25);
		panel.add(btnStandardfarbe);
		JButton btnFarbeWhlen = new JButton("Farbe w\u00E4hlen");
		btnFarbeWhlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Color backgroundColor = JColorChooser.showDialog(frame, "Choose background color", Color.white);
				if (backgroundColor != null) {
					panel.setBackground(backgroundColor);
					frame.getContentPane().setBackground(backgroundColor);
				}
			}
		});
		btnFarbeWhlen.setBounds(254, 227, 111, 25);
		panel.add(btnFarbeWhlen);
		JLabel lblAufgabe = new JLabel("Aufgabe 1: Hintergrundfarbe �ndern");
		lblAufgabe.setBounds(22, 149, 273, 16);
		panel.add(lblAufgabe);
		JLabel lblDerTextWird = new JLabel("Der Text wird ge\u00E4ndert");
		lblDerTextWird.setHorizontalAlignment(SwingConstants.CENTER);
		lblDerTextWird.setBounds(12, 13, 358, 132);
		panel.add(lblDerTextWird);
		JLabel lblAufgabeText = new JLabel("Aufgabe 2: Text formatieren");
		lblAufgabeText.setBounds(22, 265, 220, 16);
		panel.add(lblAufgabeText);
		JButton btnArial = new JButton("Arial");
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblDerTextWird.setFont(new Font("Arial", Font.PLAIN, 12));
			}
		});
		btnArial.setBounds(22, 295, 93, 25);
		panel.add(btnArial);
		JButton btnComicSans = new JButton("Comic Sans MS");
		btnComicSans.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDerTextWird.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
			}
		});
		btnComicSans.setBounds(127, 295, 115, 25);
		panel.add(btnComicSans);
		JButton btnCourierNew = new JButton("Courier New");
		btnCourierNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDerTextWird.setFont(new Font("Courier New", Font.PLAIN, 12));
			}
		});
		btnCourierNew.setBounds(256, 295, 109, 25);
		panel.add(btnCourierNew);
		txtTextEinfgen = new JTextField();
		txtTextEinfgen.setText("Text einf\u00FCgen");
		txtTextEinfgen.setBounds(22, 333, 343, 22);
		panel.add(txtTextEinfgen);
		txtTextEinfgen.setColumns(10);
		JButton btnInsLabelSchreiben = new JButton("Ins Label Schreiben");
		btnInsLabelSchreiben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDerTextWird.setText(txtTextEinfgen.getText());
			}
		});
		btnInsLabelSchreiben.setBounds(22, 368, 145, 25);
		panel.add(btnInsLabelSchreiben);
		JButton btnTextImLabel = new JButton("Text im Label l\u00F6schen");
		btnTextImLabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDerTextWird.setText("");
			}
		});
		btnTextImLabel.setBounds(197, 368, 168, 25);
		panel.add(btnTextImLabel);
		JLabel lblAufgabe_1 = new JLabel("Aufgabe 3: Schriftfarbe �ndern");
		lblAufgabe_1.setBounds(22, 416, 343, 16);
		panel.add(lblAufgabe_1);
		JButton btnRot_1 = new JButton("Rot");
		btnRot_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDerTextWird.setForeground(Color.RED);
			}
		});
		btnRot_1.setBounds(22, 445, 93, 25);
		panel.add(btnRot_1);
		JButton btnBlau_1 = new JButton("Blau");
		btnBlau_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDerTextWird.setForeground(Color.BLUE);
			}
		});
		btnBlau_1.setBounds(127, 445, 115, 25);
		panel.add(btnBlau_1);
		JButton btnSchwarz = new JButton("Schwarz");
		btnSchwarz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDerTextWird.setForeground(Color.BLACK);
			}
		});
		btnSchwarz.setBounds(254, 445, 111, 25);
		panel.add(btnSchwarz);

		JLabel lblAufgabeSchriftgre = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe �ndern");

		lblAufgabeSchriftgre.setBounds(22, 492, 343, 16);

		panel.add(lblAufgabeSchriftgre);

		JButton button = new JButton("+");

		button.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				int groesse = lblDerTextWird.getFont().getSize();

				String font = lblDerTextWird.getFont().getFamily();

				lblDerTextWird.setFont(new Font(font, Font.PLAIN, groesse + 1));

			}

		});

		button.setBounds(22, 521, 163, 25);

		panel.add(button);

		JButton button_1 = new JButton("-");

		button_1.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				int groesse = lblDerTextWird.getFont().getSize();

				String font = lblDerTextWird.getFont().getFamily();

				lblDerTextWird.setFont(new Font(font, Font.PLAIN, groesse - 1));

			}

		});

		button_1.setBounds(197, 521, 168, 25);

		panel.add(button_1);

		JLabel lblAufgabe_2 = new JLabel("Aufgabe 5: Textausrichtung");

		lblAufgabe_2.setBounds(22, 569, 343, 16);

		panel.add(lblAufgabe_2);

		JButton btnLinksbndig = new JButton("linksb\u00FCndig");

		btnLinksbndig.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				lblDerTextWird.setHorizontalAlignment(SwingConstants.LEFT);

			}

		});

		btnLinksbndig.setBounds(22, 598, 109, 25);

		panel.add(btnLinksbndig);

		JButton btnZentriert = new JButton("zentriert");

		btnZentriert.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				lblDerTextWird.setHorizontalAlignment(SwingConstants.CENTER);

			}

		});

		btnZentriert.setBounds(145, 598, 97, 25);

		panel.add(btnZentriert);

		JButton btnRechtsbndig = new JButton("rechtsb\u00FCndig");

		btnRechtsbndig.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				lblDerTextWird.setHorizontalAlignment(SwingConstants.RIGHT);

			}

		});

		btnRechtsbndig.setBounds(254, 598, 111, 25);

		panel.add(btnRechtsbndig);

		JLabel lblAufgabeProgramm = new JLabel("Aufgabe 6: Programm beenden");

		lblAufgabeProgramm.setBounds(22, 636, 343, 16);

		panel.add(lblAufgabeProgramm);

		JButton btnExit = new JButton("Exit");

		btnExit.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				System.exit(0);

			}

		});

		btnExit.setBounds(22, 665, 343, 58);

		panel.add(btnExit);

	}

}
