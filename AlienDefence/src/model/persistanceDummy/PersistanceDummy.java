package model.persistanceDummy;

import model.persistance.IAttemptPersistance;
import model.persistance.ILevelPersistance;
import model.persistance.IPersistance;
import model.persistance.ITargetPersistance;
import model.persistance.IUserPersistance;

public class PersistanceDummy implements IPersistance{

	private LevelDummy levelDummy;
	private UserDummy userDummy;
	private AttemptDummy attemptDummy;
	private TargetDummy targetDummy;

	public PersistanceDummy() {

		this.levelDummy = new LevelDummy();
		this.userDummy = new UserDummy();
		this.attemptDummy = new AttemptDummy();
		this.targetDummy = new TargetDummy();
	}

	public IAttemptPersistance getAttemptPersistance() {
		return this.attemptDummy;
	}

	public ILevelPersistance getLevelPersistance() {
		return this.levelDummy;
	}

	public ITargetPersistance getTargetPersistance() {
		return this.targetDummy;
	}

	public IUserPersistance getUserPersistance() {
		return this.userDummy;
	}


}
