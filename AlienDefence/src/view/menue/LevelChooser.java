package view.menue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import controller.AlienDefenceController;
import controller.GameController;
import controller.LevelController;
import model.Level;
import model.User;
import view.game.GameGUI;

@SuppressWarnings("serial")
public class LevelChooser extends JPanel {

	private LevelController lvlControl;
	private LeveldesignWindow leveldesignWindow;
	private JTable tblLevels;
	private DefaultTableModel jTableData;
	private List<Level> arrLevel;
	/**
	 * Create the panel.
	 * 
	 * @param leveldesignWindow
	 */
	public LevelChooser(LevelController lvlControl, final LeveldesignWindow leveldesignWindow, final AlienDefenceController alienDefenceController, int type_id) {
		this.lvlControl = lvlControl;
		this.leveldesignWindow = leveldesignWindow;
		arrLevel = this.lvlControl.readAllLevels();
		
		setLayout(new BorderLayout());
		setForeground(Color.ORANGE);
		setBackground(Color.BLACK);
		JPanel pnlButtons = new JPanel();
		pnlButtons.setBackground(Color.BLACK);
		add(pnlButtons, BorderLayout.SOUTH);


		if(type_id == 2) {
			JButton btnSpielen = new JButton("Spielen");
			btnSpielen.setForeground(Color.ORANGE);
			btnSpielen.setBackground(Color.BLACK);
			pnlButtons.add(btnSpielen);

			btnSpielen.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
	            
	            final User user = new User(1, "test", "pass");
	            Thread t = new Thread("GameThread") {

	                //@Override
	                public void run() {

	                    GameController gameController = alienDefenceController.startGame(arrLevel.get(0), user);
	                    new GameGUI(gameController).start();
	                }
	            };
	            t.start();
				}
		    });
		}
		
		if(type_id == 3) {
		JButton btnNewLevel = new JButton("Neues Level");
		btnNewLevel.setBackground(Color.BLACK);
		btnNewLevel.setForeground(Color.ORANGE);
		btnNewLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnNewLevel_Clicked();
			}
		});
		
		pnlButtons.add(btnNewLevel);
		
		JButton btnUpdateLevel = new JButton("ausgew\u00E4hltes Level bearbeiten");
		btnUpdateLevel.setBackground(Color.BLACK);
		btnUpdateLevel.setForeground(Color.ORANGE);
		btnUpdateLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnUpdateLevel_Clicked();
			}
		});
		pnlButtons.add(btnUpdateLevel);

		JButton btnDeleteLevel = new JButton("Level l\u00F6schen");
		btnDeleteLevel.setBackground(Color.BLACK);
		btnDeleteLevel.setForeground(Color.ORANGE);
		btnDeleteLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnDeleteLevel_Clicked();
			}
		});
		
		pnlButtons.add(btnDeleteLevel);
		}
		JButton btnBeenden = new JButton("Levelauswahl beenden");

		btnBeenden.setBackground(Color.BLACK);
		btnBeenden.setForeground(Color.ORANGE);
		btnBeenden.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		leveldesignWindow.setVisible(false);
		}
		});
		pnlButtons.add(btnBeenden);
		JButton btnSchliessen = new JButton("Programm schlie�en");
		btnSchliessen.setBackground(Color.BLACK);
		btnSchliessen.setForeground(Color.ORANGE);
		btnSchliessen.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		System.exit(0);
		}
		});
		pnlButtons.add(btnSchliessen);
		
		JLabel lblLevelauswahl = new JLabel("Levelauswahl");
		lblLevelauswahl.setBackground(Color.BLACK);
		lblLevelauswahl.setForeground(Color.GREEN);
		lblLevelauswahl.setFont(new Font("Arial", Font.BOLD, 18));
		lblLevelauswahl.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblLevelauswahl, BorderLayout.NORTH);
 
		JScrollPane spnLevels = new JScrollPane();
		spnLevels.setBackground(Color.BLACK);
		spnLevels.getViewport().setBackground(Color.BLACK);
		
		add(spnLevels, BorderLayout.CENTER);

		tblLevels = new JTable();
		tblLevels.setBackground(Color.BLACK);
		tblLevels.setForeground(Color.ORANGE);
		tblLevels.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		spnLevels.setViewportView(tblLevels);

		this.updateTableData();
	}

	private String[][] getLevelsAsTableModel() {
		List<Level> levels = this.lvlControl.readAllLevels();
		String[][] result = new String[levels.size()][];
		int i = 0;
		for (Level l : levels) {
			result[i++] = l.getData();
		}
		return result;
	}

	public void updateTableData() {
		this.jTableData = new DefaultTableModel(this.getLevelsAsTableModel(), Level.getLevelDescriptions());
		this.tblLevels.setModel(jTableData);
	}

	public void btnNewLevel_Clicked() {
		this.leveldesignWindow.startLevelEditor();
	}

	public void btnUpdateLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.leveldesignWindow.startLevelEditor(level_id);
	}

	public void btnDeleteLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.lvlControl.deleteLevel(level_id);
		this.updateTableData();
	}
}
