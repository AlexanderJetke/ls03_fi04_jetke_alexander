package view.menue;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Button;
import java.awt.GridLayout;
import javax.swing.JLabel;
import java.awt.TextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Checkbox;
import java.awt.Label;
import java.awt.Panel;
import javax.swing.JTextField;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JButton;

public class NewUser extends JFrame {

 private JPanel contentPane;

 /**
* Launch the application.
*/
public static void main(String[] args) {
EventQueue.invokeLater(new Runnable() {
public void run() {
try {
NewUser frame = new NewUser();
frame.setVisible(true);
} catch (Exception e) {
e.printStackTrace();
}
}
});
}

 /**
* Create the frame.
*/
public NewUser() {
setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
setBounds(100, 100, 450, 515);
contentPane = new JPanel();
contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
setContentPane(contentPane);
contentPane.setLayout(new GridLayout(0, 2, 0, 0));
Label label_5 = new Label("Vorname:");
contentPane.add(label_5);
TextField vornameTXT = new TextField();
contentPane.add(vornameTXT);
String vorname = vornameTXT.getText();
Label label_6 = new Label("Nachname:");
contentPane.add(label_6);
TextField nachnameTXT = new TextField();
contentPane.add(nachnameTXT);
String nachname = nachnameTXT.getText();
Label label_7 = new Label("Geburtsdatum:");
contentPane.add(label_7);
TextField geburtsdatumTXT = new TextField();
contentPane.add(geburtsdatumTXT);
String geburtsdatum = geburtsdatumTXT.getText();
Label label_8 = new Label("Stra\u00DFe:");
contentPane.add(label_8);
TextField stra�eTXT = new TextField();
contentPane.add(stra�eTXT);
String stra�e = stra�eTXT.getText();
Label label_9 = new Label("Hausnummer:");
contentPane.add(label_9);
TextField hausnummerTXT = new TextField();
contentPane.add(hausnummerTXT);
String hausnummer = hausnummerTXT.getText();
Label label_10 = new Label("Postleitzahl:");
contentPane.add(label_10);
TextField postleitzahlTXT = new TextField();
contentPane.add(postleitzahlTXT);
String postleitzahl = postleitzahlTXT.getText();
Label label_11 = new Label("Stadt:");
contentPane.add(label_11);
TextField stadtTXT = new TextField();
contentPane.add(stadtTXT);
String stadt = stadtTXT.getText();
Label label_12 = new Label("Benutzername:");
contentPane.add(label_12);
TextField benutzernameTXT = new TextField();
contentPane.add(benutzernameTXT);
String benutzername = benutzernameTXT.getText();
Label label_13 = new Label("Passwort:");
contentPane.add(label_13);
TextField passwordTXT = new TextField();
contentPane.add(passwordTXT);
String password = passwordTXT.getText();
Label label_14 = new Label("Gehaltsvorstellung:");
contentPane.add(label_14);
TextField gehaltsvorstellungTXT = new TextField();
contentPane.add(gehaltsvorstellungTXT);
String gehaltsvorstellung = gehaltsvorstellungTXT.getText();
Label label_15 = new Label("Beziehungsstatus:");
contentPane.add(label_15);
TextField beziehungsstatusTXT = new TextField();
contentPane.add(beziehungsstatusTXT);
String beziehungsstatus = beziehungsstatusTXT.getText();
Label label_16 = new Label("Endnote:");
contentPane.add(label_16);
TextField textField_12 = new TextField();
contentPane.add(textField_12);
JLabel label = new JLabel("");
contentPane.add(label);
JLabel label_1 = new JLabel("");
contentPane.add(label_1);
JLabel label_2 = new JLabel("");
contentPane.add(label_2);
JLabel label_3 = new JLabel("");
contentPane.add(label_3);
JButton btnNewButton = new JButton("User erstellen");
contentPane.add(btnNewButton);


JButton btnAbbrechen = new JButton("Abbrechen");
contentPane.add(btnAbbrechen);
btnAbbrechen.setBackground(Color.GRAY);
btnAbbrechen.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent e) {
		System.exit(0);
	}
});

JLabel label_4 = new JLabel("");
contentPane.add(label_4);
JLabel label_17 = new JLabel("");
contentPane.add(label_17);
}

}